package es.inftel.lumiere.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.inftel.lumiere.entity.Episode;
import es.inftel.lumiere.entity.EpisodePK;
import es.inftel.lumiere.repository.EpisodeRepository;
@Service
public class EpisodeServiceImp implements EpisodeService {


	@Autowired
	EpisodeRepository episodeRepository;
	@Override
	public Episode findOne(EpisodePK id) {
		// TODO Auto-generated method stub
		return episodeRepository.findOne(id);
	}

}
