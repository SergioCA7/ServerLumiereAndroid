package es.inftel.lumiere.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.inftel.lumiere.entity.Userepisodeview;
import es.inftel.lumiere.entity.UserepisodeviewPK;
import es.inftel.lumiere.repository.UserepisodeviewRepository;

@Service
public class UserepisodeviewImp implements UserepisodeviewService{

	@Autowired
	UserepisodeviewRepository userepisodeviewRepository;
	@Override
	public void save(Userepisodeview uepv) {
		// TODO Auto-generated method stub
		this.userepisodeviewRepository.save(uepv);
		
	}
	@Override
	public Userepisodeview findOne(UserepisodeviewPK ep) {
		// TODO Auto-generated method stub
		return this.userepisodeviewRepository.findOne(ep);
	}
	@Override
	public void delete(Userepisodeview ep) {
		// TODO Auto-generated method stub
		this.userepisodeviewRepository.delete(ep);
		
	}

}
