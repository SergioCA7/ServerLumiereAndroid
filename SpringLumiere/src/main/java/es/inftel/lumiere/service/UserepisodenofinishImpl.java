package es.inftel.lumiere.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.inftel.lumiere.entity.Userepisodenofinish;
import es.inftel.lumiere.entity.UserepisodenofinishPK;
import es.inftel.lumiere.entity.Userfilmnofinish;
import es.inftel.lumiere.entity.Users;
import es.inftel.lumiere.repository.UserepisodenofinishRepository;

@Service
public class UserepisodenofinishImpl implements UserepisodenofinishService{
	
	
	@Autowired 
	UserepisodenofinishRepository userEpisodenofinishRepository;
	
	public List<Userepisodenofinish> findEpisodiesNotFinished (Users user){
		return (List<Userepisodenofinish>) this.userEpisodenofinishRepository.findEpisodiesNotFinished(user);
		
	}

	@Override
	public Userepisodenofinish findOne(UserepisodenofinishPK id) {
		// TODO Auto-generated method stub
		return this.userEpisodenofinishRepository.findOne(id);
	}

	@Override
	public void delete(Userepisodenofinish ep) {
		// TODO Auto-generated method stub
		this.userEpisodenofinishRepository.delete(ep);
		
	}

	@Override
	public void updateTime(BigDecimal time, UserepisodenofinishPK pk) {
		// TODO Auto-generated method stub
		this.userEpisodenofinishRepository.updateTime(time, pk);
		
	}

	@Override
	public void save(Userepisodenofinish ep) {
		// TODO Auto-generated method stub
		this.userEpisodenofinishRepository.save(ep);
		
	}
	
	
}
