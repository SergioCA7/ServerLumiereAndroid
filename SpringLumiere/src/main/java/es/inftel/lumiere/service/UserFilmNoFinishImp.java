package es.inftel.lumiere.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.inftel.lumiere.entity.Userfilmnofinish;
import es.inftel.lumiere.entity.UserfilmnofinishPK;
import es.inftel.lumiere.entity.Users;
import es.inftel.lumiere.repository.UserFilmNoFinishRepository;

@Service
public class UserFilmNoFinishImp implements UserFilmNoFinishService{
	@Autowired
	UserFilmNoFinishRepository userNFR;
	@Override
	public void save(Userfilmnofinish userNF) {
		this.userNFR.save(userNF);
	}
	@Override
	public Userfilmnofinish findOne(UserfilmnofinishPK pk) {
		return this.userNFR.findOne(pk);
	}
	@Override
	public void updateTime(BigDecimal bigDecimal, UserfilmnofinishPK ufnf) {
		this.userNFR.updateTime(bigDecimal, ufnf);
	}
	@Override
	public void deleteFilm(UserfilmnofinishPK ufnf) {
		// TODO Auto-generated method stub
		this.userNFR.delete(ufnf);
		
	}
	
	@Override
	public List<Userfilmnofinish> findFilmsNotFinished(Users user) {
		return userNFR.findFilmsNotFinished(user);
	}
	
	@Override
	public boolean exists(UserfilmnofinishPK id) {
		return userNFR.exists(id);
	}
	

}
