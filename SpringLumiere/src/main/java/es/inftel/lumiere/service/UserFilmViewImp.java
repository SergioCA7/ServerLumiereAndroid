package es.inftel.lumiere.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.inftel.lumiere.entity.Userfilmview;
import es.inftel.lumiere.entity.UserfilmviewPK;
import es.inftel.lumiere.repository.UserFilmViewRepository;

@Service
public class UserFilmViewImp implements  UserFilmViewService{

	@Autowired
	UserFilmViewRepository userFilmViewRepository;
	@Override
	public void deleteUserFilmView(UserfilmviewPK ufvPK) {
		// TODO Auto-generated method stub
		userFilmViewRepository.delete(ufvPK);
		
	}
	
	public void createUserFilmView(Userfilmview ufvPK) {
		// TODO Auto-generated method stub
		userFilmViewRepository.save(ufvPK);
		
	}

}
