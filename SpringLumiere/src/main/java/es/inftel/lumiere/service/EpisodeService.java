package es.inftel.lumiere.service;

import java.math.BigDecimal;

import es.inftel.lumiere.entity.Episode;
import es.inftel.lumiere.entity.EpisodePK;

public interface EpisodeService {

	Episode findOne(EpisodePK id2);

}
