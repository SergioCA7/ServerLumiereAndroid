package es.inftel.lumiere.service;

import org.springframework.stereotype.Service;

import es.inftel.lumiere.entity.Userepisodeview;
import es.inftel.lumiere.entity.UserepisodeviewPK;

public interface UserepisodeviewService {

	void save(Userepisodeview uepv);
	Userepisodeview findOne(UserepisodeviewPK ep);
	void delete(Userepisodeview ep);
}
