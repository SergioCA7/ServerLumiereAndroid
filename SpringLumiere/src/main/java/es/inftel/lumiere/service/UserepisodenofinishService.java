package es.inftel.lumiere.service;

import java.math.BigDecimal;
import java.util.List;

import es.inftel.lumiere.entity.Userepisodenofinish;
import es.inftel.lumiere.entity.UserepisodenofinishPK;
import es.inftel.lumiere.entity.Users;


public interface UserepisodenofinishService {
	List<Userepisodenofinish> findEpisodiesNotFinished (Users user);
	Userepisodenofinish findOne(UserepisodenofinishPK id);
	void delete(Userepisodenofinish ep);
	void updateTime(BigDecimal time, UserepisodenofinishPK pk);
	void save(Userepisodenofinish ep);
}
