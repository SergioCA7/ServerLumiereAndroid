/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.inftel.lumiere.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author sergiocuenca
 */
@Embeddable
public class CommentsPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_COMMENT")
    private BigDecimal idComment;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_FILM")
    private BigDecimal idFilm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_USER")
    private BigDecimal idUser;

    public CommentsPK() {
    }

    public CommentsPK(BigDecimal idComment, BigDecimal idFilm, BigDecimal idUser) {
        this.idComment = idComment;
        this.idFilm = idFilm;
        this.idUser = idUser;
    }

    public BigDecimal getIdComment() {
        return idComment;
    }

    public void setIdComment(BigDecimal idComment) {
        this.idComment = idComment;
    }

    public BigDecimal getIdFilm() {
        return idFilm;
    }

    public void setIdFilm(BigDecimal idFilm) {
        this.idFilm = idFilm;
    }

    public BigDecimal getIdUser() {
        return idUser;
    }

    public void setIdUser(BigDecimal idUser) {
        this.idUser = idUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComment != null ? idComment.hashCode() : 0);
        hash += (idFilm != null ? idFilm.hashCode() : 0);
        hash += (idUser != null ? idUser.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CommentsPK)) {
            return false;
        }
        CommentsPK other = (CommentsPK) object;
        if ((this.idComment == null && other.idComment != null) || (this.idComment != null && !this.idComment.equals(other.idComment))) {
            return false;
        }
        if ((this.idFilm == null && other.idFilm != null) || (this.idFilm != null && !this.idFilm.equals(other.idFilm))) {
            return false;
        }
        if ((this.idUser == null && other.idUser != null) || (this.idUser != null && !this.idUser.equals(other.idUser))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.inftel.lumiere.entity.CommentsPK[ idComment=" + idComment + ", idFilm=" + idFilm + ", idUser=" + idUser + " ]";
    }
    
}
