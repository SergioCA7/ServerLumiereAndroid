/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.inftel.lumiere.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergiocuenca
 */
@Entity
@Table(name = "USERFILMNOFINISH")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Userfilmnofinish.findAll", query = "SELECT u FROM Userfilmnofinish u")
    , @NamedQuery(name = "Userfilmnofinish.findByIdFilm", query = "SELECT u FROM Userfilmnofinish u WHERE u.userfilmnofinishPK.idFilm = :idFilm")
    , @NamedQuery(name = "Userfilmnofinish.findByIdUser", query = "SELECT u FROM Userfilmnofinish u WHERE u.userfilmnofinishPK.idUser = :idUser")
    , @NamedQuery(name = "Userfilmnofinish.findByTime", query = "SELECT u FROM Userfilmnofinish u WHERE u.time = :time")
    , @NamedQuery(name = "Userfilmnofinish.findByTimestamp", query = "SELECT u FROM Userfilmnofinish u WHERE u.timestamp = :timestamp")})
public class Userfilmnofinish implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserfilmnofinishPK userfilmnofinishPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIME")
    private BigDecimal time;
    @Column(name = "TIMESTAMP")
    private BigDecimal timestamp;
    @JoinColumn(name = "ID_FILM", referencedColumnName = "ID_FILM", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Film film;
    @JoinColumn(name = "ID_USER", referencedColumnName = "ID_USER", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Users users;

    public Userfilmnofinish() {
    }

    public Userfilmnofinish(UserfilmnofinishPK userfilmnofinishPK) {
        this.userfilmnofinishPK = userfilmnofinishPK;
    }

    public Userfilmnofinish(UserfilmnofinishPK userfilmnofinishPK, BigDecimal time) {
        this.userfilmnofinishPK = userfilmnofinishPK;
        this.time = time;
    }

    public Userfilmnofinish(BigDecimal idFilm, BigDecimal idUser) {
        this.userfilmnofinishPK = new UserfilmnofinishPK(idFilm, idUser);
    }

    public UserfilmnofinishPK getUserfilmnofinishPK() {
        return userfilmnofinishPK;
    }

    public void setUserfilmnofinishPK(UserfilmnofinishPK userfilmnofinishPK) {
        this.userfilmnofinishPK = userfilmnofinishPK;
    }

    public BigDecimal getTime() {
        return time;
    }

    public void setTime(BigDecimal time) {
        this.time = time;
    }

    public BigDecimal getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(BigDecimal timestamp) {
        this.timestamp = timestamp;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userfilmnofinishPK != null ? userfilmnofinishPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Userfilmnofinish)) {
            return false;
        }
        Userfilmnofinish other = (Userfilmnofinish) object;
        if ((this.userfilmnofinishPK == null && other.userfilmnofinishPK != null) || (this.userfilmnofinishPK != null && !this.userfilmnofinishPK.equals(other.userfilmnofinishPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.inftel.lumiere.entity.Userfilmnofinish[ userfilmnofinishPK=" + userfilmnofinishPK + " ]";
    }
    
    public String getTimeFormat() {
		String res = "0";
		if(this.time != null) {
			res = this.millisecondToString(this.time.longValue());
		}
		System.out.println(res);
		return res;  
	 		
}
    
    private  String millisecondToString(long millisecond) {
	    DecimalFormat decimalFormat = new DecimalFormat("00");
    int hours = (int) (millisecond / (3600 * 1000));
    int remaining = (int) (millisecond % (3600 * 1000));

    int minutes = (remaining / (60 * 1000));
    remaining = (remaining % (60 * 1000));

    int seconds = (remaining / 1000);

    String text = "";

    text += decimalFormat.format(hours) + ":";
    text += decimalFormat.format(minutes) + ":";
    text += decimalFormat.format(seconds);
    return text;


}
    public String barTimeCalculation() {
		Double timeDouble = this.time.doubleValue();
		Double durationDouble = this.film.getDuration()*60000;
		Double multiply = timeDouble * 100;
		Double resul = multiply / durationDouble;
		return resul.toString();
}
    
}
