/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.inftel.lumiere.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sergiocuenca
 */
@Entity
@Table(name = "USEREPISODENOFINISH")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Userepisodenofinish.findAll", query = "SELECT u FROM Userepisodenofinish u")
    , @NamedQuery(name = "Userepisodenofinish.findByIdUser", query = "SELECT u FROM Userepisodenofinish u WHERE u.userepisodenofinishPK.idUser = :idUser")
    , @NamedQuery(name = "Userepisodenofinish.findByIdEpisode", query = "SELECT u FROM Userepisodenofinish u WHERE u.userepisodenofinishPK.idEpisode = :idEpisode")
    , @NamedQuery(name = "Userepisodenofinish.findByIdSeason", query = "SELECT u FROM Userepisodenofinish u WHERE u.userepisodenofinishPK.idSeason = :idSeason")
    , @NamedQuery(name = "Userepisodenofinish.findByIdSeries", query = "SELECT u FROM Userepisodenofinish u WHERE u.userepisodenofinishPK.idSeries = :idSeries")
    , @NamedQuery(name = "Userepisodenofinish.findByTime", query = "SELECT u FROM Userepisodenofinish u WHERE u.time = :time")
    , @NamedQuery(name = "Userepisodenofinish.findByTimestamp", query = "SELECT u FROM Userepisodenofinish u WHERE u.timestamp = :timestamp")})
public class Userepisodenofinish implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserepisodenofinishPK userepisodenofinishPK;
    @Column(name = "TIME")
    private BigDecimal time;
    @Column(name = "TIMESTAMP")
    private BigDecimal timestamp;
    @JoinColumns({
        @JoinColumn(name = "ID_EPISODE", referencedColumnName = "ID_EPISODE", insertable = false, updatable = false)
        , @JoinColumn(name = "ID_SEASON", referencedColumnName = "ID_SEASON", insertable = false, updatable = false)
        , @JoinColumn(name = "ID_SERIES", referencedColumnName = "ID_SERIES", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Episode episode;
    @JoinColumn(name = "ID_USER", referencedColumnName = "ID_USER", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Users users;

    public Userepisodenofinish() {
    }

    public Userepisodenofinish(UserepisodenofinishPK userepisodenofinishPK) {
        this.userepisodenofinishPK = userepisodenofinishPK;
    }

    public Userepisodenofinish(BigDecimal idUser, BigDecimal idEpisode, BigDecimal idSeason, BigDecimal idSeries) {
        this.userepisodenofinishPK = new UserepisodenofinishPK(idUser, idEpisode, idSeason, idSeries);
    }

    public UserepisodenofinishPK getUserepisodenofinishPK() {
        return userepisodenofinishPK;
    }

    public void setUserepisodenofinishPK(UserepisodenofinishPK userepisodenofinishPK) {
        this.userepisodenofinishPK = userepisodenofinishPK;
    }

    public BigDecimal getTime() {
        return time;
    }

    public void setTime(BigDecimal time) {
        this.time = time;
    }

    public BigDecimal getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(BigDecimal timestamp) {
        this.timestamp = timestamp;
    }

    public Episode getEpisode() {
        return episode;
    }

    public void setEpisode(Episode episode) {
        this.episode = episode;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userepisodenofinishPK != null ? userepisodenofinishPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Userepisodenofinish)) {
            return false;
        }
        Userepisodenofinish other = (Userepisodenofinish) object;
        if ((this.userepisodenofinishPK == null && other.userepisodenofinishPK != null) || (this.userepisodenofinishPK != null && !this.userepisodenofinishPK.equals(other.userepisodenofinishPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.inftel.lumiere.entity.Userepisodenofinish[ userepisodenofinishPK=" + userepisodenofinishPK + " ]";
    }
    
    
    public String barTimeCalculation() {
    		Double timeDouble = this.time.doubleValue();
    		Double durationDouble = (this.episode.getDuration().doubleValue())*60000;
    		Double multiply = timeDouble * 100;
    		Double resul = multiply / durationDouble;
    		return resul.toString();
    }
}
