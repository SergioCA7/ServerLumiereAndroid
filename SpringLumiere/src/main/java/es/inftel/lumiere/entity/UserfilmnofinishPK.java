/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.inftel.lumiere.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author sergiocuenca
 */
@Embeddable
public class UserfilmnofinishPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_FILM")
    private BigDecimal idFilm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_USER")
    private BigDecimal idUser;

    public UserfilmnofinishPK() {
    }

    public UserfilmnofinishPK(BigDecimal idFilm, BigDecimal idUser) {
        this.idFilm = idFilm;
        this.idUser = idUser;
    }

    public BigDecimal getIdFilm() {
        return idFilm;
    }

    public void setIdFilm(BigDecimal idFilm) {
        this.idFilm = idFilm;
    }

    public BigDecimal getIdUser() {
        return idUser;
    }

    public void setIdUser(BigDecimal idUser) {
        this.idUser = idUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFilm != null ? idFilm.hashCode() : 0);
        hash += (idUser != null ? idUser.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserfilmnofinishPK)) {
            return false;
        }
        UserfilmnofinishPK other = (UserfilmnofinishPK) object;
        if ((this.idFilm == null && other.idFilm != null) || (this.idFilm != null && !this.idFilm.equals(other.idFilm))) {
            return false;
        }
        if ((this.idUser == null && other.idUser != null) || (this.idUser != null && !this.idUser.equals(other.idUser))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.inftel.lumiere.entity.UserfilmnofinishPK[ idFilm=" + idFilm + ", idUser=" + idUser + " ]";
    }
    
}
