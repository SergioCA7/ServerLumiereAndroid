/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.inftel.lumiere.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author sergiocuenca
 */
@Embeddable
public class UserepisodenofinishPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_USER")
    private BigDecimal idUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_EPISODE")
    private BigDecimal idEpisode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_SEASON")
    private BigDecimal idSeason;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_SERIES")
    private BigDecimal idSeries;

    public UserepisodenofinishPK() {
    }

    public UserepisodenofinishPK(BigDecimal idUser, BigDecimal idEpisode, BigDecimal idSeason, BigDecimal idSeries) {
        this.idUser = idUser;
        this.idEpisode = idEpisode;
        this.idSeason = idSeason;
        this.idSeries = idSeries;
    }

    public BigDecimal getIdUser() {
        return idUser;
    }

    public void setIdUser(BigDecimal idUser) {
        this.idUser = idUser;
    }

    public BigDecimal getIdEpisode() {
        return idEpisode;
    }

    public void setIdEpisode(BigDecimal idEpisode) {
        this.idEpisode = idEpisode;
    }

    public BigDecimal getIdSeason() {
        return idSeason;
    }

    public void setIdSeason(BigDecimal idSeason) {
        this.idSeason = idSeason;
    }

    public BigDecimal getIdSeries() {
        return idSeries;
    }

    public void setIdSeries(BigDecimal idSeries) {
        this.idSeries = idSeries;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUser != null ? idUser.hashCode() : 0);
        hash += (idEpisode != null ? idEpisode.hashCode() : 0);
        hash += (idSeason != null ? idSeason.hashCode() : 0);
        hash += (idSeries != null ? idSeries.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserepisodenofinishPK)) {
            return false;
        }
        UserepisodenofinishPK other = (UserepisodenofinishPK) object;
        if ((this.idUser == null && other.idUser != null) || (this.idUser != null && !this.idUser.equals(other.idUser))) {
            return false;
        }
        if ((this.idEpisode == null && other.idEpisode != null) || (this.idEpisode != null && !this.idEpisode.equals(other.idEpisode))) {
            return false;
        }
        if ((this.idSeason == null && other.idSeason != null) || (this.idSeason != null && !this.idSeason.equals(other.idSeason))) {
            return false;
        }
        if ((this.idSeries == null && other.idSeries != null) || (this.idSeries != null && !this.idSeries.equals(other.idSeries))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.inftel.lumiere.entity.UserepisodenofinishPK[ idUser=" + idUser + ", idEpisode=" + idEpisode + ", idSeason=" + idSeason + ", idSeries=" + idSeries + " ]";
    }
    
}
