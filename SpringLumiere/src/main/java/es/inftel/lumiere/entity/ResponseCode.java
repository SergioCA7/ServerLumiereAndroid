package es.inftel.lumiere.entity;

public final class ResponseCode {

	public static final Integer CREATED = 201;
	public static final Integer OK = 200;
	public static final Integer SERVERERROR = 500;

	
}
