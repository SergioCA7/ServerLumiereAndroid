package es.inftel.lumiere.rest;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.inftel.lumiere.entity.Episode;
import es.inftel.lumiere.entity.EpisodePK;
import es.inftel.lumiere.entity.Film;
import es.inftel.lumiere.entity.Series;
import es.inftel.lumiere.entity.Userepisodenofinish;
import es.inftel.lumiere.entity.UserepisodenofinishPK;
import es.inftel.lumiere.entity.Userepisodeview;
import es.inftel.lumiere.entity.UserepisodeviewPK;
import es.inftel.lumiere.entity.Userfilmnofinish;
import es.inftel.lumiere.entity.UserfilmnofinishPK;
import es.inftel.lumiere.entity.Userfilmview;
import es.inftel.lumiere.entity.UserfilmviewPK;
import es.inftel.lumiere.entity.Users;
import es.inftel.lumiere.service.EpisodeService;
import es.inftel.lumiere.service.SerieService;
import es.inftel.lumiere.service.UserService;
import es.inftel.lumiere.service.UserepisodenofinishService;
import es.inftel.lumiere.service.UserepisodeviewService;

@RestController
@RequestMapping(value="/seriesController")
public class SeriesControllerRest {

	@Autowired
	SerieService seriesService;
	
	@Autowired
	UserepisodenofinishService episodeNFService;
	
	@Autowired
	UserepisodeviewService  episodeViewService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	EpisodeService episodeService;
	
	@RequestMapping(value = "/getLastestSeries", method = RequestMethod.GET)
	public List<Series> getLastestSeries()
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		return seriesService.findLatestSeries();
	}
	
	@RequestMapping(value = "/getSeriesById/{serieId}", method = RequestMethod.GET)
	public Series getSerieById(@PathVariable("serieId") String serieId)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		return seriesService.findSerieByID(new BigDecimal(serieId));
	}
	
	@RequestMapping(value = "/getSeriesByTitle/{title}", method = RequestMethod.GET)
	public List<Series> getSeriesByTitle(@PathVariable("title") String title)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		return seriesService.getSeriesByTitle(title.toUpperCase());
	}
	
	@RequestMapping(value = "/getEpisodesNotFinished/{userId}", method = RequestMethod.GET)
	public List<Userepisodenofinish> getEpisodesNotFinished(@PathVariable("userId") String userId)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		return episodeNFService.findEpisodiesNotFinished(userService.findById(new BigDecimal(userId)));
	}
	
	@RequestMapping(value = "/filmViewUpdate/{idEpisode}/{idSeasson}/{idSerie}/{idUser}", method = RequestMethod.GET)
	@ResponseBody
	public String  updateTime( @PathVariable("idEpisode") String idEpisode,@PathVariable("idSeries") String idSeries,@PathVariable("idSeasson") String idSeasson, @PathVariable("idUser") String idUser,HttpServletRequest request) {
		
		String res = "0";
		Users user = userService.findById(new BigDecimal(idUser));
	    UserepisodenofinishPK userENPK = new UserepisodenofinishPK(user.getIdUser(),new BigDecimal(idEpisode),new BigDecimal(idSeasson),new BigDecimal(idSeries));
		Userepisodenofinish userENF = episodeNFService.findOne(userENPK);
		if(userENF != null) {
			res = userENF.getTime().toString();
		}
		System.out.print("Iba por el milisecond " + res);
		return res;
		
	}
	
	@RequestMapping(value = "/filmViewNoFinish/{idEpisode}/{idSeasson}/{idSerie}/{idUser}/{time}", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public void videoNoFinish( @PathVariable("idEpisode") String idEpisode,@PathVariable("idSeasson") String idSeason,@PathVariable("idSerie") String idSerie,@PathVariable("idUser") String idUser,@PathVariable("time") String time,HttpServletRequest request) {
			System.out.println("PASO POR AQUI, TIEMPO  " +  time);

			
			Users user = userService.findById(new BigDecimal(idUser));
			UserepisodenofinishPK userEPNFPK = new UserepisodenofinishPK(new BigDecimal(idUser),new BigDecimal(idEpisode),new BigDecimal(idSeason),new BigDecimal(idSerie));
			Userepisodenofinish userEPNF = this.episodeNFService.findOne(userEPNFPK);
			Double mins = new Double(time);
			System.out.println("VIDEO PARADO " + mins.toString());
			Episode ep = this.episodeService.findOne(new EpisodePK(new BigDecimal(idEpisode), new BigDecimal(idSeason), new BigDecimal(idSerie)));
			Double duration = ep.getDuration().doubleValue();
			duration = duration*60000;
			System.out.println("VIDEO DURACION " + duration);

			if(mins >= duration) {
				Userepisodeview userEpisodeView = new Userepisodeview( new BigDecimal(idUser),  new BigDecimal(idSeason),  new BigDecimal(idEpisode),  new BigDecimal(idSerie));
				episodeViewService.save(userEpisodeView);
				if(userEPNF != null) {
					this.episodeNFService.delete(userEPNF);
				}
			} else {
				if(userEPNF != null) {
					this.episodeNFService.updateTime(new BigDecimal(time),userEPNF.getUserepisodenofinishPK());
				} else {
					userEPNF = new Userepisodenofinish(userEPNFPK);
					userEPNF.setTime(new BigDecimal(time));
					this.episodeNFService.save(userEPNF);	
				}
				
				Userepisodeview userEpisodeView = this.episodeViewService.findOne(new UserepisodeviewPK(new BigDecimal(idUser),  new BigDecimal(idSeason),  new BigDecimal(idEpisode),  new BigDecimal(idSerie)));
				if(userEpisodeView != null) {
					this.episodeViewService.delete(userEpisodeView);
				}
			}
	}
	
	@RequestMapping(value = "/getEpisodesById/{id_serie}", method = RequestMethod.GET)
	public List<Episode> getEpisodesById(@PathVariable("id_serie") String id_serie)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		return seriesService.getEpisodesById(new BigDecimal(id_serie));
	}
	
	
}
