package es.inftel.lumiere.rest;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import es.inftel.lumiere.entity.Users;
import es.inftel.lumiere.service.FilmService;
import es.inftel.lumiere.service.SerieService;
import es.inftel.lumiere.service.UserFilmNoFinishService;
import es.inftel.lumiere.service.UserService;
import es.inftel.lumiere.service.UserepisodenofinishService;

@RestController
@RequestMapping(value="/userController")
public class UserControllerRest {
	@Autowired
	UserService userService;

	@Autowired
	UserepisodenofinishService userepisodenofinishService;
	
	@Autowired
	FilmService filmService;
	
	@Autowired
	UserFilmNoFinishService userfilmnofinishService;
	
	@Autowired
	SerieService seriesService;
	
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public Users register(@RequestBody Users user)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		if (userService.checkUserEmail(user.getEmail()) == null) {
			user.setAnswer("-");
			userService.save(user);
		}
		return user;
	}			
			

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public Users login(@RequestBody Users userLogin, HttpServletRequest request)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		Users user = userService.getUser(userLogin.getEmail(), userLogin.getPass());
		if (user != null) {
			request.getSession().setAttribute("loggedUser", user);  
		}
		return user;
	}
	
	@RequestMapping(value = "/login/{userEmail}/{pass}", method = RequestMethod.GET)
	public Users login(@PathVariable("userEmail") String userEmail,@PathVariable("pass") String userPass,HttpServletRequest request)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		System.out.println("Email " + userEmail);
		System.out.println("Pass " + userPass);

		Users user = userService.getUser(userEmail,userPass);
		if (user != null) {
			request.getSession().setAttribute("loggedUser", user);  
		}
		return user;
		
	}
	

	@RequestMapping(method = RequestMethod.GET)
	public List<Users> getAllUsers () {
		return userService.getAllUsers();
	}

	@RequestMapping(value = "/users/{userId}", method = RequestMethod.GET)
	public Users getUser (@PathVariable ("userId")  BigDecimal userId)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		return userService.findById(userId);
		
	}
	
	@RequestMapping(value = "/users/{userId}", method = RequestMethod.PATCH)
	public Users updateUser(@RequestBody Users user) {
		Users userEdit = userService.findById(user.getIdUser());
		userEdit.setEmail(user.getEmail());
		if (user != null) {
			 userService.updatePass(user.getPass(), userEdit.getIdUser());
		}
		return user;
	}
	
	
	
	
	
	
}
