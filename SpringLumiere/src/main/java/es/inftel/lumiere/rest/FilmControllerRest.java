package es.inftel.lumiere.rest;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import es.inftel.lumiere.entity.Film;
import es.inftel.lumiere.entity.Users;
import es.inftel.lumiere.repository.UserFilmViewRepository;
import es.inftel.lumiere.entity.Userfilmnofinish;
import es.inftel.lumiere.entity.UserfilmnofinishPK;
import es.inftel.lumiere.entity.Userfilmview;
import es.inftel.lumiere.entity.UserfilmviewPK;
import es.inftel.lumiere.service.FilmService;
import es.inftel.lumiere.service.UserFilmNoFinishService;
import es.inftel.lumiere.service.UserService;

@RestController
@RequestMapping(value="/filmController")
public class FilmControllerRest {

	
	@Autowired
	UserService userService;
	
	@Autowired
	FilmService filmService;
	
	@Autowired
	UserFilmNoFinishService userfilmnofinishService;
	
	@Autowired
    private STResourceHttpRequestHandler handler;
	
	@Autowired
	UserFilmNoFinishService userFNFS;
	
	@Autowired
	UserFilmViewRepository userFV;

	
	@CrossOrigin
    @GetMapping("/stream/{title}")
    public void home(HttpServletRequest request, HttpServletResponse response, @PathVariable("title") String title) throws ServletException, IOException {
		System.out.println("Titulo1 " + title);
		if(title.contains("%20")) {
			title.replaceAll("%20", "");
		}
		title = title.replaceAll("\u00A0", "");
		title = title.replaceAll(" ", "");
		title = title.replaceAll(":", "");
	
		String loc = "/Users/inftel07/Desktop/pelis/" + title + ".mp4";
		File MP4_FILE = new File(loc);
		System.out.println("Titulo " + title);
		if(!MP4_FILE.exists()){
			System.out.println("Titulo No encontrado");
			//File por defecto
			 loc = "/Users/inftel07/Desktop/pelis/crop.mp4";
			 MP4_FILE = new File(loc);
		}
        request.setAttribute(STResourceHttpRequestHandler.ATTR_FILE, MP4_FILE);
        handler.handleRequest(request, response);
    }
		
	
	@RequestMapping(value = "/getFilmById/{filmId}", method = RequestMethod.GET)
	public Film getFilmByFilmId(@PathVariable("filmId") String filmId)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		
		return filmService.findFilmByID(new BigDecimal(filmId));
	}
	
	@RequestMapping(value = "/getFilmNotFinished/{userId}", method = RequestMethod.GET)
	public List<Userfilmnofinish> getFilmNotFinished(@PathVariable("userId") String userId)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		Users user = userService.findById(new BigDecimal(userId));
		System.out.println("Film not finish");

		
		return (List<Userfilmnofinish>) userfilmnofinishService.findFilmsNotFinished(user);
	}
	
	@RequestMapping(value = "/getFilmsByUser/{userId}/", method = RequestMethod.GET)
	public List<Userfilmnofinish> getFilmsByUserId(@PathVariable("userId") BigDecimal userId)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		Users user = userService.findById(userId);
		return userfilmnofinishService.findFilmsNotFinished(user);
	}
	
	@RequestMapping(value = "/getFilmsByTitle/{introducedTitle}", method = RequestMethod.GET)
	public List<Film> getFilmsByTitle(@PathVariable("introducedTitle") String introducedTitle)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		return filmService.getFilmsByTitle(introducedTitle.toUpperCase());
		
	}
	
	@RequestMapping(value = "/getFilmsByDirector/{introducedDirector}", method = RequestMethod.GET)
	public List<Film> getFilmsByDirector(@PathVariable("introducedDirector") String introducedDirector)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		return filmService.getFilmsByDirector(introducedDirector.toUpperCase());
		
	}
	
	@RequestMapping(value = "/getRated", method = RequestMethod.GET)
	public List<Film> getFilmsByUserId ()
		throws NoSuchAlgorithmException, InvalidKeySpecException {
		return filmService.findTopRatedFilmsFilmaffinityUsers();
	}
	
	@RequestMapping(value = "/getLastestFilms", method = RequestMethod.GET)
	public List<Film> getLastestFilms()
		throws NoSuchAlgorithmException, InvalidKeySpecException {
		return filmService.findLatestFilms();
	}
	
	@RequestMapping (value = "/{genres}",  method = RequestMethod.GET)
	public List<Film> getFilmsByGenres (@PathVariable("genres") String genres){
		return filmService.findFilmByGenresMap().getOrDefault(genres, Collections.emptyList());
	}
	
	@RequestMapping(value = "/filmViewUpdate/{idFilm}/{idUser}", method = RequestMethod.GET)
	@ResponseBody
	public String  updateTime( @PathVariable("idFilm") String idFilm, @PathVariable("idUser") String idUser,HttpServletRequest request) {
		String res = "0";
		Film  film = this.filmService.findOne(new BigDecimal(idFilm));
		Users user = userService.findById(new BigDecimal(idUser));
	    UserfilmnofinishPK userFNPK = new UserfilmnofinishPK(film.getIdFilm(),user.getIdUser());
		Userfilmnofinish userFN = this.userFNFS.findOne(userFNPK);
		if(userFN != null){
			res = userFN.getTime().toString();
		}
		System.out.print("Iba por el milisecond " + res);
		return res;
	}
	
	@RequestMapping(value = "/filmViewNoFinish/{idFilm}/{idUser}/{time}", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public void videoNoFinish( @PathVariable("idFilm") String idFilm,@PathVariable("idUser") String idUser,@PathVariable("time") String time,HttpServletRequest request) {
			System.out.println("PASO POR AQUI, TIEMPO  " +  time);

			Film  film = this.filmService.findOne(new BigDecimal(idFilm));
			Users user = userService.findById(new BigDecimal(idUser));
			UserfilmnofinishPK userFNPK = new UserfilmnofinishPK(film.getIdFilm(),user.getIdUser());
			Userfilmnofinish userFN = this.userFNFS.findOne(userFNPK);
			Double mins = new Double(time);
			System.out.println("VIDEO PARADO " + mins.toString());
			Double duration = film.getDuration();
			duration = duration*60000;
			System.out.println("VIDEO DURACION " + duration);

			if(mins >= duration) {
				Userfilmview userFilmView = new Userfilmview(user.getIdUser(),film.getIdFilm());
				this.userFV.save(userFilmView);
				if(userFN != null) {
					this.userFNFS.deleteFilm(userFNPK);
				}
			} else {
				if(userFN != null) {
					this.userFNFS.updateTime(new BigDecimal(time),userFN.getUserfilmnofinishPK());
				} else {
					userFN = new Userfilmnofinish(userFNPK,new BigDecimal(time));
					this.userFNFS.save(userFN);	
				}
				Userfilmview userFilmView = this.userFV.findOne(new UserfilmviewPK(user.getIdUser(),film.getIdFilm()));
				if(userFV != null) {
					this.userFV.delete(userFilmView);
				}
			}
	}
	
	
	@Component
    final static class STResourceHttpRequestHandler extends ResourceHttpRequestHandler {

        final static String ATTR_FILE = STResourceHttpRequestHandler.class.getName() + ".file";

        @Override
        protected Resource getResource(HttpServletRequest request) throws IOException {

            final File file = (File) request.getAttribute(ATTR_FILE);
            return new FileSystemResource(file);
        }
    }

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public Film insertFilm(@RequestBody Film film) {
		if (filmService.checkTitle(film.getTitle()) == null) {
			filmService.save(film);
		}
		return film;
	}			
		
	@RequestMapping(value = "/films" , method = RequestMethod.GET)
	public List<Film> getAllFilms() {
		return filmService.getAllFilms();
	}
	
	@RequestMapping(value = "/films/{filmId}", method = RequestMethod.GET)
	public Film getFilm (@PathVariable ("filmId")  BigDecimal filmId){
		return filmService.findFilmByID(filmId);
		
	}
	
	/*@RequestMapping(value = "/films/{filmId}", method = RequestMethod.PATCH)
	public Film updateFilm(@RequestBody Film film) {
		Film filmEdit = filmService.findFilmByID(film.getIdFilm());
		filmEdit.setImagePoster(film.getImagePoster());;
		filmEdit.setImageNetflix(film.getImageNetflix());
		if (film != null) {
			 filmService.updateFilm(film.getIdFilm());
			//filmService.save(filmEdit);
		}
		return filmEdit;
	}*/
	
	
}
