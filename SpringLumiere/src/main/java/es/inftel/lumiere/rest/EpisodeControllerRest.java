package es.inftel.lumiere.rest;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import es.inftel.lumiere.entity.Film;
import es.inftel.lumiere.entity.Userepisodenofinish;
import es.inftel.lumiere.entity.Users;
import es.inftel.lumiere.entity.Userfilmnofinish;
import es.inftel.lumiere.service.FilmService;
import es.inftel.lumiere.service.UserFilmNoFinishService;
import es.inftel.lumiere.service.UserService;
import es.inftel.lumiere.service.UserepisodenofinishService;

@RestController
@RequestMapping(value="/episodeController")
public class EpisodeControllerRest {

	
	@Autowired
	UserService userService;
	
	@Autowired
	UserepisodenofinishService userepisodenofinishService;
	
	@RequestMapping(value = "/getNotFinishedEpisodesByUserId/{userId}/", method = RequestMethod.GET)
	public List<Userepisodenofinish> getNotFinishedEpisodesByUserId(@PathVariable("userId") BigDecimal userId)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		Users user = userService.findById(userId);
		return userepisodenofinishService.findEpisodiesNotFinished(user);
	}

}