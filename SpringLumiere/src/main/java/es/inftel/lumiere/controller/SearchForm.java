package es.inftel.lumiere.controller;

public class SearchForm {
	
	private String text;
	
	public SearchForm (String text) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

}
