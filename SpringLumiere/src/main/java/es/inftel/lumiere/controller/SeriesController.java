package es.inftel.lumiere.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.inftel.lumiere.controller.FilmController.STResourceHttpRequestHandler;
import es.inftel.lumiere.entity.Series;
import es.inftel.lumiere.entity.Users;
import es.inftel.lumiere.service.SerieService;

@Controller


public class SeriesController {
	
	@Autowired
    private STResourceHttpRequestHandler handler;
	
	@Autowired
	SerieService serieService;
	
	

	@RequestMapping(value = "/series", method = RequestMethod.GET)
	public String goSeries (Model model, HttpServletRequest request) {
		String res = "home";
		Users user = (Users) request.getSession().getAttribute("loggedUser");
		if(user != null){
			Map<String, List<Series>> seriesMap = serieService.findSerieByGenresMap();
			model.addAttribute("seriesMap", seriesMap);
			res = "series";
		}
		return res;
	}
	
	
	@RequestMapping(value = "/serieView/{id}", method = RequestMethod.GET)
	public String showSerie (Model modelo, @PathVariable("id") BigDecimal id, HttpServletRequest request) {
		String res = "home";
		Users user = (Users) request.getSession().getAttribute("loggedUser");
		if(user != null){
			modelo.addAttribute("serie", serieService.findSerieByID(id));
			res = "serieView";
		}
		return res;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/serieStream/{title}", method = RequestMethod.GET)
	public void stream (Model modelo, @PathVariable("title") String title, HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		if(title.contains("%20")) {
			title.replaceAll("%20", "");
		}
		title = title.replaceAll("\u00A0", "");
		title = title.replaceAll(" ", "");
		title = title.replaceAll(":", "");
		String loc = "/Users/inftel01/Documents/Proyecto Web/LumiereSpring/SpringLumiere/src/main/webapp/resources/images/" + title + ".mp4";
		File MP4_FILE = new File(loc);
		if(!MP4_FILE.exists()){
			//File por defecto
			 loc = "/Users/inftel01/Documents/Proyecto Web/LumiereSpring/SpringLumiere/src/main/webapp/resources/images/crop.mp4";
			 MP4_FILE = new File(loc);
		}
        request.setAttribute(STResourceHttpRequestHandler.ATTR_FILE, MP4_FILE);
        handler.handleRequest(request, response);
	}
	
	
}
