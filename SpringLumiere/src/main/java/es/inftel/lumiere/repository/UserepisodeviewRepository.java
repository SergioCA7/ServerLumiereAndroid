package es.inftel.lumiere.repository;

import org.springframework.data.repository.CrudRepository;

import es.inftel.lumiere.entity.Userepisodenofinish;
import es.inftel.lumiere.entity.UserepisodenofinishPK;
import es.inftel.lumiere.entity.Userepisodeview;
import es.inftel.lumiere.entity.UserepisodeviewPK;

public interface UserepisodeviewRepository extends CrudRepository <Userepisodeview,UserepisodeviewPK>{

}
