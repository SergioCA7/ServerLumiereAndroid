package es.inftel.lumiere.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import es.inftel.lumiere.entity.Securityquestion;
import es.inftel.lumiere.entity.Userepisodenofinish;
import es.inftel.lumiere.entity.Userfilmnofinish;
import es.inftel.lumiere.entity.UserfilmnofinishPK;
import es.inftel.lumiere.entity.Users;

public interface UserFilmNoFinishRepository extends CrudRepository<Userfilmnofinish, UserfilmnofinishPK>{

	@Transactional
	@Modifying
	@Query("UPDATE Userfilmnofinish U SET U.time =?1 WHERE U.userfilmnofinishPK =?2")
	void updateTime(BigDecimal time, UserfilmnofinishPK ufnf);
	
	@Query("SELECT enf FROM Userepisodenofinish enf WHERE enf.users =?1 ORDER BY timestamp DESC")
	List<Userepisodenofinish> findEpisodiesNotFinished (Users user);
	
	@Query("SELECT fnf FROM Userfilmnofinish fnf WHERE fnf.users =?1 ORDER BY timestamp DESC")
	List<Userfilmnofinish> findFilmsNotFinished (Users user);

}
