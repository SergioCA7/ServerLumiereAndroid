package es.inftel.lumiere.repository;

import java.math.BigDecimal;

import org.springframework.data.repository.CrudRepository;
import es.inftel.lumiere.entity.Securityquestion;


public interface SecurityRepository extends CrudRepository<Securityquestion, BigDecimal>{

	
}
