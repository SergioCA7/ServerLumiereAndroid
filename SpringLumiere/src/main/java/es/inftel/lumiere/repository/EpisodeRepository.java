package es.inftel.lumiere.repository;

import java.math.BigDecimal;

import org.springframework.data.repository.CrudRepository;

import es.inftel.lumiere.entity.EpisodePK;
import es.inftel.lumiere.entity.Episode;

public interface EpisodeRepository extends CrudRepository<Episode, EpisodePK>{

}
