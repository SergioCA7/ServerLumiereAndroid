<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>    
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>

<head>
<title>Lumière</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="resources/assets/css/main.css" />
<jsp:include page="inc/netflix.jsp"></jsp:include>
</head>


<body>
	<!-- Header -->
	<header id="header" class="alt">
		<div class="logo">
			<a href="/index"><img src="resources/images/lumiere.png"
				style="width: 110px; height: 70px;" /></a>
		</div>
		<div class="navBar">
			<a href="/series">Series</a>
		</div>
		<div class="navBar">
			<a href="#">Films</a>
		</div>
		<div class="navBar">
			<input id="searchInput" type="text" style="width: 400px; height: 40px; color: aqua; font-weight: bold;"
				placeholder="Movies, series, directors, ..." />
		</div>
		<div class="navBar">
			<button style="width: 100px; height: 40px" class="button" onclick="auxSearch()">Search</button>
		</div>
		<a href="#menu">Menu</a>
	</header>


	<!-- Nav -->
	<nav class="navBar" id="menu">
		<ul class="links">
			<li><a href="/myProfile">My profile</a></li>
			<li><a href="/logOut">Log out</a></li>
		</ul>
	</nav>


	<!-- Header quote -->
	<section id="two" class="wrapper style3">
		<div class="inner">
			<header class="align-center">
				<br>
				<p>
					<b>All you need is a sofa, a blanket and a good movie!</b>
				</p>
			</header>
		</div>
	</section>


	<section class="content" style="background-color: #071418">
	
		<!-- Genres container -->
		<c:forEach items="${genres}" var="genre">
		<div class="container">
			<header>
				<h2>
					<b style="color: #FAFAFA">${genre.key}</b>
				</h2>
			</header>
			<div class="rowN">
				<div class="rowN__inner">
				<c:forEach items="${genre.value}" var="film">
					<div class="tile">
						<div class="tile__media">
							<img class="tile__img" src="${film.imageNetflix}" />
						</div>
						<div class="tile__details">
							<a href="/filmView/${film.idFilm}"><div class="tile__title">${film.title}</div></a>
						</div>
					</div>
				</c:forEach>

				</div>
			</div>
		</div>
		</c:forEach>
	</section>


	<!-- Footer -->
	<footer id="footer">
		<div class="container" style="display: none">
			<ul class="icons">
				<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
				<li><a href="#" class="icon fa-facebook"><span
						class="label">Facebook</span></a></li>
				<li><a href="#" class="icon fa-instagram"><span
						class="label">Instagram</span></a></li>
				<li><a href="#" class="icon fa-envelope-o"><span
						class="label">Email</span></a></li>
			</ul>
		</div>
		<div class="copyright">&copy; Lumière. All rights reserved.</div>
	</footer>

	<!-- Scripts -->
	<script src="resources/assets/js/jquery.min.js"></script>
	<script src="resources/assets/js/jquery.scrollex.min.js"></script>
	<script src="resources/assets/js/skel.min.js"></script>
	<script src="resources/assets/js/util.js"></script>
	<script src="resources/assets/js/main.js"></script>
	<script src="resources/assets/js/searchJS.js"></script>
	
	<div id="hiddenSearch" style="display: none;">
        		<form:form id="formSearch" method="GET">
        			<button id="buttonSearch" style="width: 100px; height: 40px" class="button" >Search</button>
        		</form:form>
        </div>
</body>
</html>
