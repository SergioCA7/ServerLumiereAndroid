<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>

<html>
<head>
<title>Lumière</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="../resources/assets/css/main.css" />
</head>
<body>

	<!-- Header -->
	<header id="header" class="alt">
		<div class="logo">
			<a href="/index"><img src="../resources/images/lumiere.png"
				style="width: 110px; height: 70px;" /></a>
		</div>
		<div class="navBar">
			<a href="/series">Series</a>
		</div>
		<div class="navBar">
			<a href="/films">Films</a>
		</div>
		<div class="navBar">
			<input id="searchInput" type="text" style="width: 400px; height: 40px; color: aqua; font-weight: bold;"
				placeholder="Movies, series, directors, ..." />
		</div>
		<div class="navBar">
			<button style="width: 100px; height: 40px" class="button" onclick="auxSearch()">Search</button>
		</div>
		<a href="#menu">Menu</a>
	</header>


	<!-- Nav -->
	<nav class="navBar" id="menu">
		<ul class="links">
			<li><a href="/myProfile">My profile</a></li>
			<li><a href="/logOut">Log out</a></li>
		</ul>
	</nav>


	<!-- Film title -->
	<section id="two" class="wrapper style3">
		<div class="inner">
			<header class="align-center">
				<br>
				<h2>
					<b><div id="filmTitle">${film.title}</div></b>
				</h2>
			</header>
		</div>
	</section>


	<!-- Film quote -->
	<section id="three" class="wrapper style2">
		<div class="inner">
			<header class="align-center">
				<p class="special" style="color: #0A2A29">"${film.quote}"</p>
				<h1>${film.quoteAuthor}</h1>
			</header>
		</div>
	</section>


	<!-- Film details -->
	<section id="one" class="wrapper style2">
		<div class="inner">
			<div class="grid-style">
				<!-- Poster container -->
				<div>
					<div class="box" style="background-color: #0A2A29">
						<div class="image fit">
							<img src="${film.imagePoster}" alt="" />
						</div>

						<div class="content">
							<header class="align-center">
								<p>
									<b>Popularity</b>
								</p>
							</header>
							<p style="text-align: center">${film.ourRating}</p>

						</div>
					</div>
				</div>
				<!-- Details container -->
				<div>
					<div class="box" style="background-color: #0A2A29">
						<div class="content">
							<header class="align-center">
								<p>
									<b>Sinopsis</b>
								</p>
							</header>
							<p style="text-align: justify">
								(${film.origen}, ${film.year}) <br>${film.sinopsis}</p>
							<header class="align-center">
								<p>
									<b>Director</b>
								</p>
							</header>
							<p style="text-align: center">${film.director}</p>
							<header class="align-center">
								<p>
									<b>Duration</b>
								</p>
							</header>
							<p style="text-align: center">${film.duration}min</p>
						</div>
						<div class="image fit" style="text-align: center">
							<iframe id="BOOM" width="600" height="448"
								src="https://www.youtube.com/embed/${film.ytLink}?autoplay=0"></iframe>
						</div>
						<div class="content">
                                <footer class="align-center">
                                    <a id="playVideo" href="#img1" class="button alt" style="background-color: white" onclick="videoPetition()"><b>Watch it now</b></a>
                                    
                                    <a href="#" class="lightbox" id="img1" onclose="closeVideo()" onclick="closeVideo()">
									<div id="videoModal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" aria-hidden="false" style="display: block;">
									  <div class="modal-header">
									    <button type="button" class="close full-height" data-dismiss="modal" aria-hidden="true" onclick="closeVideo()">X</button>
									    <h3 style="color: white;" id="title">${film.title}</h3>
									  </div>
									  <div class="modal-body"> <video id="videoStreaming" width="870" height="489"controls><source src=""></video>
									  </div>
									  <div class="modal-footer"></div>
									</div>
                                </footer>
                            </div>
					</div>
				</div>
			</div>
		</div>
	</section>

	
		


	<!-- Footer -->
	<footer id="footer">
		<div class="container" style="display: none">
			<ul class="icons">
				<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
				<li><a href="#" class="icon fa-facebook"><span
						class="label">Facebook</span></a></li>
				<li><a href="#" class="icon fa-instagram"><span
						class="label">Instagram</span></a></li>
				<li><a href="#" class="icon fa-envelope-o"><span
						class="label">Email</span></a></li>
			</ul>
		</div>
		<div class="container align-center">
			<script type='text/javascript'>
				var title = $("#title").text();

				var titleFilm = "'" + title + "'";
				var searchTitle = "'" + title + " available films'";

				var amzn_wdgt = {
					widget : 'Carousel'
				};
				amzn_wdgt.tag = 'amawid-21';
				amzn_wdgt.widgetType = 'SearchAndAdd';
				amzn_wdgt.searchIndex = 'DVD';
				amzn_wdgt.title = searchTitle;
				amzn_wdgt.width = '600';
				amzn_wdgt.keywords = titleFilm;
				amzn_wdgt.height = '200';
				amzn_wdgt.marketPlace = 'GB';
				amzn_wdgt.shuffleProducts = 'True';
			</script>
			<script type='text/javascript'
				src='http://wms-eu.amazon-adsystem.com/20070822/GB/js/swfobject_1_5.js'>
				
			</script>
		</div>
		<div class="copyright">&copy; Lumière. All rights reserved.</div>
	</footer>

<div id="idFilm" style="display: none;">${film.idFilm}</div>
	<!-- Scripts -->
	<script src="../resources/assets/js/jquery.min.js"></script>
	<script src="../resources/assets/js/jquery.scrollex.min.js"></script>
	<script src="../resources/assets/js/skel.min.js"></script>
	<script src="../resources/assets/js/util.js"></script>
	<script src="../resources/assets/js/main.js"></script>
    <script src="../resources/assets/js/streaming.js"></script>
    <script src="../resources/assets/js/searchJS.js"></script>
	
	<div id="hiddenSearch" style="display: none;">
        		<form:form id="formSearch" method="GET">
        			<button id="buttonSearch" style="width: 100px; height: 40px" class="button" >Search</button>
        		</form:form>
   </div>
</body>
</html>